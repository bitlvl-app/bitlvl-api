import { version } from '../../package.json'
import { Router } from 'express'
import facets from './facets'
import users from './users'
import auth from './auth'
import countries from './countries'
import roles from './roles'

export default ({ config, db }) => {
	let api = Router();
	
	api.use('/facets', facets({ config, db }));

	api.use('/users', users({ config, db }));

	api.use('/countries', countries({ config, db }));

	api.use('/roles', roles({ config, db }));

	api.use('/auth', auth({ config, db }));

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
}
