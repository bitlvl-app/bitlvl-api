import resource from 'resource-router-middleware'
import { Countries } from '../models'
import moment from 'moment'
import Jwt from '../jwt'

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id: 'countryName',

	load({ req }, id, callback) {
		callback(req, id);
	},

	/** GET /:id - Return a given entity */
	read({ countryName }, res) {
		Countries.findOne({ name: countryName }).exec((err, country) => {
			if (err) return err

			if (country) {
				res.status(200).json(country)
			} else {
				res.status(404).json({ status: 404, message: 'Nothing was found' })
			}
		});
	},

	index({ params }, res) {
		Countries.find().select('-_id').exec((err, countries) => {
			if (err) return err

			if (countries) {
				res.status(200).json(countries)
			} else {
				res.status(404).json({ status: 404, message: 'Nothing was found' })
			}
		})
	},
});
