import resource from 'resource-router-middleware'
import { User, Countries } from '../../models'
import moment from 'moment'
import Jwt from '../../jwt'
import utils from './utils'

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id: 'email',

	load({ req }, id, callback) {
		callback(req, id);
	},

	/** GET /:id - Return a given entity */
	async read({ email }, res) {
		try {
			const response = await utils.getUser(email)
			res.status(response.status).json(response.responseBody)
		} catch (err) {
			res.status(err.status).json(err.responseBody)
		}
	},

	/** GET / - List all entities */
	index({ params }, res) {
		res.json([{ user: 1 }])
	},

	/** POST / - Create a new entity */
	async create({ body }, res) {
		try {
			const response = await utils.postUser(body)
			res.status(response.status).json(response.responseBody)
		}
		catch (err) {
			res.status(err.status).json(err.responseBody)
		}
	},

	/** PUT /:id - Update a given entity */
	async update({ email, body }, res) {
		try {
			const response = await utils.putUser(email, body)
			res.status(response.status).json(response.responseBody)
		} catch (err) {
			res.status(err.status).json(err.responseBody)
		}
	},

	/** DELETE /:id - Delete a given entity */
	async delete({ email }, res) {
		try {
			const response = await utils.deleteUser(email)
			res.status(response.status).json(response.responseBody)
		} catch (err) {
			res.status(err.status).json(err.responseBody)
		}
	}
});
