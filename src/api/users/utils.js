import resource from 'resource-router-middleware'
import { User, Countries, Roles } from '../../models'
import moment from 'moment'


const responseFormatter = (status, message) => (
    {
        status,
        responseBody: {
            status,
            message
        }
    }
)


const utils = {
    postUser(user) {
        return new Promise((resolve, reject) => {
            Roles.findOne({ roleId: Number.parseFloat(user.role) }, (err, role) => {
                if (!role) return reject(responseFormatter(422, `${user.role} is not an available role`))
                Countries.findOne({ name: user.country }, (err, country) => {
                    if (!country) return reject(responseFormatter(422, `${user.country} is not an available country`))

                    User({
                        firstName: user.firstName,
                        lastName: user.lastName,
                        birth: moment(user.birth, 'YYYY-MM-DD').toDate(),
                        creationDate: Date.now(),
                        role: role._id,
                        email: user.email,
                        password: user.password,
                        phone: user.phone,
                        country: country._id,
                        address: user.address
                    }).save((err) => {
                        if (err) return reject(responseFormatter(500, err))                        
                        return resolve(responseFormatter(200, 'Success'))
                    })
                })
            })
        })
    },

    getUser(email) {
        return new Promise((resolve, reject) => {
            User.findOne({ email }).populate('country role').exec((err, user) => {
                if (err) return reject(responseFormatter(500, err))
                if (user) {
                    return resolve(responseFormatter(200, user))
                } else {
                    return resolve(responseFormatter(404, 'Nothing was found'))
                }
            })
        })
    },

    putUser(email, newData) {
        return new Promise((resolve, reject) => {
            User.findOne({ email }, (err, user) => {
                if (err) return reject(responseFormatter(404, err));
                if (!user) return reject(responseFormatter(404, 'User not found'))


                if (newData.firstName) user.set({ firstName: newData.firstName })
                if (newData.lastName) user.set({ lastName: newData.lastName })
                if (newData.birth) user.set({ birth: moment(newData.birth, 'DD/MM/YYYY').toDate() })
                if (newData.role) user.set({ role: newData.role })
                if (newData.email) user.set({ email: newData.email })
                if (newData.password) user.set({ password: newData.password })
                if (newData.phone) user.set({ phone: newData.phone })

                user.save((err, updatedUser) => {
                    if (err) return reject(responseFormatter(404, err));
                    resolve(responseFormatter(200, 'User updated'))
                })
            })
        })
    },

    deleteUser(email) {
        return new Promise((resolve, reject) => {
            User.findOne({ email }, (err, user) => {
                if (err) return reject(responseFormatter(404, err))
                if (!user) return reject(responseFormatter(404, 'User not found'))

                user.remove((err) => {
                    if (err) reject(responseFormatter(404, err))
                    resolve(responseFormatter(200, 'User removed'))
                })
            })
        })
    }
}

export default utils
