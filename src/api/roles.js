import resource from 'resource-router-middleware'
import { Roles } from '../models'
import moment from 'moment'
import Jwt from '../jwt'

export default ({ config, db }) => resource({

    /** Property name to store preloaded entity on `request`. */
    id: 'roleId',

    load({ req }, id, callback) {
        callback(req, id);
    },

    index({ params }, res) {
        Roles.find().select('-_id').exec((err, roles) => {
            if (err) return err

            if (roles) {
                res.status(200).json(roles)
            } else {
                res.status(404).json({ status: 404, message: 'Nothing was found' })
            }
        })
    },
});
