import resource from 'resource-router-middleware'
import { User } from '../../models'
import moment from 'moment'
import jwt from '../../jwt'
import bcrypt from 'bcrypt'

const responseFormatter = (status, message) => (
    {
        status,
        responseBody: {
            status,
            message
        }
    }
)

const payloadFormatter = (user) => (
    {
        name: `${user.firstName} ${user.lastName}`,
        role: '1',
    }
)

const jwtResponse = (user) => (jwt.sign(payloadFormatter(user), 100000000))


const utils = {
    checkUser(data) {
        return new Promise((resolve, reject) => {
            User.findOne({ email: data.email }).exec((err, user) => {
                if (err) return reject(responseFormatter(500, err))
                if (user) {
                    bcrypt.compare(data.password, user.password, (err, same) => {
                        if (err) reject(responseFormatter(500, err))

                        if (same) {
                            return resolve(responseFormatter(200, jwtResponse(user)))
                        } else {
                            return reject(responseFormatter(401, 'Unauthorized'))
                        }
                    })
                } else {
                    return resolve(responseFormatter(404, 'Nothing was found'))
                }
            })
        })
    },
}

export default utils