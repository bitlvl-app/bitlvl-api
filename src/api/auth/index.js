import resource from 'resource-router-middleware'
import { User } from '../../models'
import utils from './utils'

export default ({ config, db }) => resource({
	/** POST / - Authenticate */
	async create({ body }, res) {
		console.log("Called auth!")
		try {
			const result = await utils.checkUser(body)
			res.status(result.status).json(
				result.responseBody
				//jwt.sign(body, 1000)
			)
		} catch (err) {
			res.status(err.status).json(
				err.responseBody
				//jwt.sign(body, 1000)
			)
		}
	}
});