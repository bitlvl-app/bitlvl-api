import config from './config'
import mongoose from 'mongoose'

const roleSchema = new config.Schema({
    roleId: {
        type: Number, required: true, index: { unique: true }
    },
    name: {
        type: String, required: true, index: { unique: true }
    }
})

const Roles = mongoose.model('Role', roleSchema)

export default Roles