import config from './config'
import mongoose from 'mongoose'

const countrySchema = new config.Schema({
    name: {
        type: String, required: true, index: { unique: true }
    }
})

const Countries = mongoose.model('Country', countrySchema)

export default Countries