import config from './config'
import mongoose from 'mongoose'
import bcrypt from 'bcrypt'

const saltRounds = 11

const userSchema = new config.Schema({
    firstName: {
        type: String, required: true
    },
    lastName: {
        type: String, required: true
    },
    birth: {
        type: Date, required: true
    },
    creationDate: {
        type: Date, required: true
    },
    role: {
        type: config.ObjectID, ref: 'Role', required: true
    },
    email: {
        type: String, required: true, index: { unique: true }
    },
    password: {
        type: String, required: true
    },
    phone: {
        type: String, required: true
    },
    country: {
        type: config.ObjectID, ref: 'Country', required: true
    },
    address: {
        type: String, required: true
    },
})


userSchema.pre('save', function(next) {
    let user = this

    if (!user.isModified('password')) return next()
    
    bcrypt.genSalt(saltRounds, (err, salt) => {
        if (err) return next(err)
        
        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) return next(err)
            user.password = hash
            next()
        })
    })
})

const Users = mongoose.model('User', userSchema)

export default Users