import mongoose from 'mongoose'

const Schema = mongoose.Schema
const ObjectID = Schema.ObjectId

const config = { Schema, ObjectID }

export default config