db.roles.insertMany(
    [
        {
            roleId: 1,
            name: "Admin"
        },
        {
            roleId: 2,
            name: "Manager"
        },
        {
            roleId: 3,
            name: "Analyst"
        }
    ]
);