import mongoose from 'mongoose'

const auth = {
	user: process.env.MONGO_DEV_USER,
	pwd: process.env.MONGO_DEV_PWD
}

const mongoDB = `mongodb://${auth.user}:${auth.pwd}@localhost:27017/bitlvl-api`

export default callback => {
	// connect to a database if needed, then pass it to `callback`:
	mongoose.connect(mongoDB);

	mongoose.Promise = global.Promise;

	const db = mongoose.connection;

	
	callback(db);
}
