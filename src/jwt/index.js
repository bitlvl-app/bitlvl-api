import jwt from 'jsonwebtoken'
import { Users } from '../models'

const key = 'JWT%$2018BITLVL'

export default class Jwt {

    static sign(data, exp) {
        console.log(data)
        console.log(exp)
        return jwt.sign(data, key, { expiresIn: exp })
    }

    static check(token) {
        return jwt.verify(token, key, (err, decoded) => {
            return decoded ? decoded : err
        });
    }
}