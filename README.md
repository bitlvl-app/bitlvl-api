BitLvL High Level API - Customer and device data
==================================

# Install dependencies
npm install

# Start development live-reload server
PORT=8080 npm run dev

# Start production server:
PORT=8080 npm start
```
Docker Support
------
```sh
cd bitlvl-api

# Build your docker
docker build -t bitlvl/api .
#            ^      ^           ^
#          tag  tag name      Dockerfile location

# run your docker
docker run -p 8080:8080 bitlvl/api
#                 ^            ^
#          bind the port    container tag
#          to your host
#          machine port   

```